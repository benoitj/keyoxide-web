<?php $this->layout('template.base', ['title' => $title]) ?>

<div class="content">
    <h1>Guides</h1>

    <div class="guides">
        <div class="guides__section">
            <h3>Using Keyoxide</h3>
            <a href="/guides/verify">Verifying a signature</a><br>
            <a href="/guides/encrypt">Encrypting a message</a><br>
            <a href="/guides/proofs">Verifying identity proofs</a><br>
            <a href="/guides/contributing">Contributing to Keyoxide</a><br>
            <a href="/guides/self-hosting-keyoxide">Self-hosting Keyoxide</a><br>
        </div>

        <div class="guides__section">
            <h3>OpenPGP and identity proofs</h3>
            <a href="/guides/openpgp-proofs">How OpenPGP identity proofs work</a><br>
            <a href="/guides/web-key-directory">Uploading keys using web key directory</a><br>
        </div>

        <div class="guides__section">
            <h3>Adding proofs</h3>
            <a href="/guides/devto">Dev.to</a><br>
            <a href="/guides/discourse">Discourse</a><br>
            <a href="/guides/dns">Domain / DNS</a><br>
            <a href="/guides/github">Github</a><br>
            <a href="/guides/hackernews">Hackernews</a><br>
            <a href="/guides/lobsters">Lobste.rs</a><br>
            <a href="/guides/mastodon">Mastodon</a><br>
            <a href="/guides/pleroma">Pleroma</a><br>
            <a href="/guides/reddit">Reddit</a><br>
            <a href="/guides/twitter">Twitter</a><br>
            <a href="/guides/xmpp">XMPP+OMEMO</a><br>
        </div>

        <div class="guides__section">
            <h3>Other services</h3>
            <a href="/guides/feature-comparison-keybase">Feature comparison with Keybase</a><br>
            <a href="/guides/migrating-from-keybase">Migrating from Keybase</a><br>
        </div>

        <div class="guides__section">
            <h3>Managing proofs in GnuPG</h3>
            <a href="/guides/managing-proofs-listing">Listing proofs</a><br>
            <a href="/guides/managing-proofs-deleting">Deleting proofs</a><br>
        </div>
    </div>
</div>
