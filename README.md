# Keyoxide

[Keyoxide](https://keyoxide.org) is a modern, secure and decentralized platform to prove your online identity.

[![Build Status](https://drone.keyoxide.org/api/badges/keyoxide/web/status.svg?branch=main)](https://drone.keyoxide.org/keyoxide/web)
